(function () {
  // Replace background by img in .page-section--slide 
  $('.maxwidth-banner').each(function(){
        var section = $(this).find('.for-bg').attr('src');
        
        if(section == true){
            $(this).css({'background-color':'#fff'});
    
        }
        else{
            $(this).css({'background-image':'url(' + section + ')'});
      
        }
    });
  // End of replace background by img in .page-section--slide
})();